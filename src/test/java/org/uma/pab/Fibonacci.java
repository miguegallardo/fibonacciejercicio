package org.uma.pab;


/**

 * Created by Migue on 07/03/2017.

 */

public class Fibonacci {



    public static int[] compute(int n, int numRequested)

    {

        if (numRequested > 41){

            throw new IllegalArgumentException("Month values have to be lower than 40");

        }

        else if (n < 2 || n > 5){

            throw new IllegalArgumentException("Newborn rabbits values have to be between 2 and 5");

        }

        return compute((n == 2) ? new int[] { 2, 1 } : compute(n - 1, n), numRequested);

    }



    public static int[] compute(int[] startingValues, int numRequested)

    {

        int[] output = new int[numRequested];

        int n = startingValues.length;

        System.arraycopy(startingValues, 0, output, 0, n);

        for (int i = n; i < numRequested; i++)

            for (int j = 1; j <= n; j++)

                output[i] += output[i - j];

        return output;

    }

}