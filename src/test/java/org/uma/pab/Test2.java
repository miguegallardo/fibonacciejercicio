package org.uma.pab;

import static org.uma.pab.Fibonacci.compute;

/**
 * Created by Migue on 07/03/2017.
 */
public class Test2 {
    public static void main(String[] args)
    {
        int meses = 41;
        int crias = 3;

        for (int value : compute(crias, meses+1)){
            System.out.print(" " + value);
            System.out.println();
        }
    }
}
