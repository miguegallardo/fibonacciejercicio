package org.uma.pab;

import static org.uma.pab.Fibonacci.compute;

/**
 * Created by Migue on 07/03/2017.
 */
public class Test3 {
    public static void main(String[] args)
    {
        int meses = 5;
        int crias = 6;

        for (int value : compute(crias, meses)){
            System.out.print(" " + value);
            System.out.println();
        }
    }
}

